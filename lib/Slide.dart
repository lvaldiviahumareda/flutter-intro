class Slide{
  String _title;

  Slider(String t, String i){
    this._title =t;
    this._image = i;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  String _image;

  String get image => _image;

  set image(String value) {
    _image = value;
  }

}