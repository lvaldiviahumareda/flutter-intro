import 'package:flutter/material.dart';
import 'package:charla/Slide.dart';


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Presentación',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController pageController;
  int currentPage = 0;

  _MyHomePageState() {
    pageController = PageController(initialPage: currentPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Drawer"),
      ),
      body: PageView(
        controller: pageController,
        children: <Widget>[
          MyHomeScreen(),
          MyMessagesScreen(),
          MyFavoritesScreen(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentPage,
        onTap: (index) {
          currentPage = index;
          this.pageController.animateToPage(currentPage,
              duration: Duration(milliseconds: 300), curve: Curves.ease);
          setState(() {

          });
        },
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Homes'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.mail),
            title: Text('Messages'),
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.favorite), title: Text("Favoritos"))
        ],
      ),
      drawer: Drawer(
          child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Luis"),
            accountEmail: Text("valdivialuis1989@gmail.com"),
            currentAccountPicture: CircleAvatar(),
          ),
          Column(
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.person_outline),
                title: Text("perfil"),
              ),
              ListTile(
                leading: Icon(Icons.info),
                title: Text("Información"),
              ),
              ListTile(
                leading: Icon(Icons.map),
                title: Text("Mi Ruta"),
              )
            ],
          ),
        ],
      )),
    );
  }
}

class MyHomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyHomeScreenState();
}

class MyHomeScreenState extends State<MyHomeScreen> {
  @override
  Widget build(BuildContext context) {
    List<String> slides = ["Slide1", "Slide2", "Slide3"];
    return Scaffold(
        body: ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return Text(slides[index]);
            },
            itemCount: slides.length));
  }

  void _setData(){
    Slide slide = Slide(t: "slide1",i: "");
  }
}

class MyMessagesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyMessagesScreenState();
}

class MyMessagesScreenState extends State<MyMessagesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Text("messages"));
  }
}

class MyFavoritesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyFavoritesScreenState();
}

class MyFavoritesScreenState extends State<MyFavoritesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Text("favorites"));
  }
}

